﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _416
{
    class Program
    {
        static void Main(string[] args)
        {
            double skruga, skvadr;

            Console.Write("Введите площадь круга: ");
            skruga = Convert.ToDouble(Console.ReadLine());
            Console.Write("Введите площадь квадрата: ");
            skvadr = Convert.ToDouble(Console.ReadLine());

            if (skruga > skvadr)
            {
                Console.WriteLine("Квадрат уместится в круге");
            }
            else if (skruga < skvadr)
            {
                Console.WriteLine("Круг уместится в квадрате");
            }
            else
            {
                Console.WriteLine("Площади равны");
            }
            Console.ReadKey();
        }
    }
}
