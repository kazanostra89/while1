﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace z433
{
    class Program
    {
        static void Main(string[] args)
        {
            int N;
            Console.Write("Введите натуральное число: ");
            N = Convert.ToInt32(Console.ReadLine());

            int B = N % 10;

            if (B % 2 == 0)
            {
                Console.WriteLine("Заканчивается четной цифрой");
            }
            else
            {
                Console.WriteLine("Заканчивается нечетной цифрой");
            }

            Console.ReadKey();
        }
    }
}
