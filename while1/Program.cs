﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace while1
{
    class Program
    {
        static void Main(string[] args)
        {
            int i;
            int finishI;
            int startI;
            int step;
            Console.WriteLine("startI ");
            startI = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("finishI ");
            finishI = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("step ");
            step = Convert.ToInt32(Console.ReadLine());
            while (i <= finishI)
            {
                Console.WriteLine($"{i} - hello");
                i += step;
            }

            Console.ReadLine();
        }
    }
}
