﻿using static System.Console;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main()
        {
            int zp1, zp2, zp3, max;
            max = 0;
            Write("Введите зарплату первого сотрудника: ");
            zp1 = int.Parse(ReadLine());
            WriteLine();
            Write("Введите зарплату второго сотрудника: ");
            zp2 = int.Parse(ReadLine());
            WriteLine();
            Write("Введите зарплату третьего сотрудника: ");
            zp3 = int.Parse(ReadLine());
            WriteLine();

            if (zp1 > zp2 && zp1 > zp3)
            {
                 max = zp1;
            }
            else if (zp2 > zp1 && zp2 > zp3)
            {
                max = zp2;
            }
            else if (zp3 > zp1 && zp3 > zp2)
            {
                    max = zp3;
            }
            WriteLine(max);
            ReadKey();
        }
    }
}
