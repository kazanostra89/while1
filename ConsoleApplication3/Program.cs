﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {
            double number1, number2;
            char action;

            Console.Write("Введите первое число: ");
            number1 = double.Parse(Console.ReadLine());

            Console.Write("Введите второе число: ");
            number2 = double.Parse(Console.ReadLine());

            Console.Write("Введите действие (+ - * /): ");
            action = char.Parse(Console.ReadLine());

            switch (action)
            {
                case '+':
                    Console.WriteLine(number1 + number2);
                    break;
                case '-':
                    Console.WriteLine(number1 - number2);
                    break;
                case '*':
                    Console.WriteLine(number1 * number2);
                    break;
                case '/':
                    if (number2 == 0)
                    {
                        Console.WriteLine("oshibka");
                    }
                    else
                    {
                        Console.WriteLine(number1 / number2);
                    }
                    break;
                default:
                    Console.WriteLine("Действие не распознано");
                    break;
            }



                Console.ReadKey();
        }
    }
}
