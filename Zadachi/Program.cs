﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadachi
{
    class Program
    {
        static void Main(string[] args)
        {
            int N;
            Console.Write("Введите натуральное число: ");
            N = Convert.ToInt32(Console.ReadLine());

            if ((N % 2) == 0)
            {
                Console.WriteLine("Введенное число четное");
            }
            else
            {
                Console.WriteLine("Введенное число нечетное");
            }
            if (N % 10 == 7)
            {
                Console.WriteLine("Оканчивается на 7");
            }
            else
            {
                Console.WriteLine("Не оканчивается на 7");
            }
            

            Console.ReadKey();
        }
    }
}
