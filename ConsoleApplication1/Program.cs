﻿using static System.Console;


namespace ConsoleApplication1
{
    class Program
    {
        static void Main()
        {
            char simvol;
            Write("Введите символ для проверки = ");
            simvol = char.Parse(ReadLine());

            if (simvol >= 'а' && simvol <= 'я' || simvol >= 'А' && simvol <= 'Я')
            {
                WriteLine("Это буква");
                if (simvol >= 'а' && simvol <= 'я')
                {
                    WriteLine("Это буква маленькая");
                }
                else
                {
                    WriteLine("Это буква большая");
                }
            }
            else if (simvol >= '0' && simvol <= '9')
            {
                WriteLine("Это символ");
                if (simvol % 2 == 0)
                {
                    WriteLine("четная");
                }
                else
                {
                    WriteLine("нечетная");
                }
            }
            else
            {
                WriteLine("Неизвестный символ");
            }

            ReadKey();
        }
    }
}
