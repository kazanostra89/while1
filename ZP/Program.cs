﻿using System;

namespace ZP
{
    class Program
    {
        static void Main()
        {
            int x, y, z, max, min;
            // ввод данных
            Console.Write("Введите натуральное число x: ");
            x = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите натуральное число y: ");
            y = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите натуральное число z: ");
            z = Convert.ToInt32(Console.ReadLine());
            /*
            Console.WriteLine();
            Console.WriteLine($"Проверка введенных чисел {x}, {y}, {z} на соответствие условию не превышения значения более 100,000");
            // проверка
            Console.WriteLine("Проверка числа х-->");
            if (x >= Math.Pow(10, 5))
            {
                Console.Write("Превышение значения! Введите повторно x меньше 100,000: ");
                x = Convert.ToInt32(Console.ReadLine());
            }
            else
            {
                Console.WriteLine("Число х = " + x + " прошло проверку");
                Console.WriteLine("Проверяем число y-->");
            }
            if (y >= Math.Pow(10, 5))
            {
                Console.Write("Превышение значения! Введите повторно y меньше 100,000: ");
                y = Convert.ToInt32(Console.ReadLine());
            }
            else
            {
                Console.WriteLine("Число y = " + y + " прошло проверку");
                Console.WriteLine("Проверяем число z-->");
            }
            if (z >= Math.Pow(10, 5))
            {
                Console.Write("Превышение значения! Введите повторно z меньше 100,000: ");
                z = Convert.ToInt32(Console.ReadLine());
            }
            else
            {
                Console.WriteLine("Проверка прошла успешно");
            }
            */            
            max = x;
            if (max > y && max > z)
            {
            Console.WriteLine("Число х = " + x + " наибольшее");
            }
            else if (y > max && y > z)
            {
            max = y;
            Console.WriteLine("Число y = " + y + " наибольшее");
            }
            else
            {
            max = z;
            Console.WriteLine("Число z = " + z + " наибольшее");
            }

            min = x;
            if (min < y && min < z)
            {
            Console.WriteLine("Число х = " + x + " наименьшее");
            }
            else if (y < min && y < z)
            {
            min = y;
            Console.WriteLine("Число y = " + y + " наименьшее");
            }
            else
            {
            min = z;
            Console.WriteLine("Число z = " + z + " наименьшее");
            }

            Console.Write("Разница между MAX и MIN = ");
            Console.WriteLine(max - min);
            
            Console.ReadKey();
            
        }
    }
}
